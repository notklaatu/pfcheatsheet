# Pathfinder RPG DM Cheatsheets

Running an RPG is a lot of fun, but there's a lot to remember.

These are cheatsheets for DMs running a [Pathfinder RPG
game](http://paizo.com/pathfinderRPG/prd/coreRulebook/gettingStarted.html)
game.


## How do I download?

If you want to download the cheatsheets, grab them from the dist
folder. I render at least to PDF, possibly other formats, too.


## How do I contribute?

If you see a problem, oversight, error, or you just have a friendly
suggestion, file a bug here in the Gitlab issue tracker.

If you want to make a cheatsheet and contribute, I'll happily accept
contributions. These documents were created on [Slackware
Linux](http://slackware.com) with
[Dia](https://wiki.gnome.org/Apps/Dia/), so I do require that you send
me ``.dia`` files.


## What are these licensed?

These documents are Creative Commons licensed. See the LICENSE file for full details.